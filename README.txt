DESCRIPTION
===========

This module adds the header and sliding region (and its contents) of the rootcandy theme to your default theme.


INSTALLATION
============
0. Install rootcandy theme and set it to be admin theme.

1. Install & enable the module.

2. Set user rights for the roles which should be able to see the header on your main theme.

3. Add

   <?php if (!empty($rootcandy_header)) print $rootcandy_header; ?>

   to your page.tpl.php of the main theme (that's not rootcandy!) right after the opening <body> tag.
    (If rootcandy is your main theme, you don't need this module)

not required
4. Add blocks to the sliding region in admin/build/block - module is using rootcandy blocks settings.

ABOUT
=====
Inspired by the admin module (http://drupal.org/project/admin)
Code adapted from rootcandy, admin module and the block module.


CONTRIBUTORS
============
Raimund Hofmann drupal@raimund-hofmann.de
Marek Sotak @sign - http://drupal.org/user/37679
