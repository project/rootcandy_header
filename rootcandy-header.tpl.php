    <div id="toppanel">
      <div id="panel">
       <?php print $slider ?>
        <div id="slider-left">
          <?php if ($slider_left) print $slider_left; ?>
        </div>
         <div id="slider-right">
          <?php if ($slider_right) print $slider_right; ?>
        </div>
        <div id="slider-middle">
          <?php if ($slider_middle) print $slider_middle; ?>
        </div>
      </div>
      <div id="toppanel-head">
        <div id="go-home">
          <?php print $go_admin; ?>
        </div>
        <div id="admin-links">
          <?php print $user_links ?>
        </div>
        <?php if (!$hide_panel) { ?>
        <div id="header-title" class="clearfix">
          <ul id="toggle"><li><?php print $panel_navigation ?></li></ul>
        </div>
        <?php } ?>
      </div>